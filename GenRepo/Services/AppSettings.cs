﻿namespace GenRepo.Services
{
    public class AppSettings
    {
        public string ConnectionStringDefault { get; set; }
        public bool SeedData { get; set; }
        public string SeedPath { get; set; }
        public string ControllerScaffoldConfig { get; set; }
        public string RepositoryScaffoldConfig { get; set; }
        public bool CreateScaffolds { get; set; }
        public string BaseDirectory { get; set; }

        public AppSettings(){}
    }
}

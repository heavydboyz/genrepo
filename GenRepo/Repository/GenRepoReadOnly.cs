﻿using GenRepo.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GenRepo.Repository 
{
    public class GenRepoReadOnly<TContext> : IGenRepoReadOnly
    where TContext : DbContext
    {
        protected readonly TContext context;

        public GenRepoReadOnly(TContext context)
        {
            this.context = context;
        }

        protected virtual IQueryable<TCommonEntity> GetQueryable<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity
        {
            IQueryable<TCommonEntity> query = context.Set<TCommonEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var navPath in includeProperties)
                {
                    query = query.Include(navPath);
                }
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            return query;
        }

        public virtual IEnumerable<TCommonEntity> GetAll<TCommonEntity>(
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity
        {
            return GetQueryable<TCommonEntity>(null, orderBy, includeProperties, skip, take).ToList();
        }

        public virtual async Task<IEnumerable<TCommonEntity>> GetAllAsync<TCommonEntity>(
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity
        {
            return await GetQueryable<TCommonEntity>(null, orderBy, includeProperties, skip, take).ToListAsync();
        }

        public virtual IEnumerable<TCommonEntity> Get<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity
        {
            return GetQueryable<TCommonEntity>(filter, orderBy, includeProperties, skip, take).ToList();
        }

        public virtual async Task<IEnumerable<TCommonEntity>> GetAsync<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity
        {
            return await GetQueryable<TCommonEntity>(filter, orderBy, includeProperties, skip, take).ToListAsync();
        }

        public virtual TCommonEntity GetOne<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            IEnumerable<string> includeProperties = null)
            where TCommonEntity : class, ICommonEntity
        {
            return GetQueryable<TCommonEntity>(filter, null, includeProperties).SingleOrDefault();
        }

        public virtual async Task<TCommonEntity> GetOneAsync<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            IEnumerable<string> includeProperties = null)
            where TCommonEntity : class, ICommonEntity
        {
            return await GetQueryable<TCommonEntity>(filter, null, includeProperties).SingleOrDefaultAsync();
        }

        public virtual TCommonEntity GetFirst<TCommonEntity>(
           Expression<Func<TCommonEntity, bool>> filter = null,
           Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
           IEnumerable<string> includeProperties = null)
           where TCommonEntity : class, ICommonEntity
        {
            return GetQueryable<TCommonEntity>(filter, orderBy, includeProperties).FirstOrDefault();
        }

        public virtual async Task<TCommonEntity> GetFirstAsync<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null)
            where TCommonEntity : class, ICommonEntity
        {
            return await GetQueryable<TCommonEntity>(filter, orderBy, includeProperties).FirstOrDefaultAsync();
        }

        public virtual TCommonEntity GetById<TCommonEntity>(object id)
            where TCommonEntity : class, ICommonEntity
        {
            return context.Set<TCommonEntity>().Find(id);
        }

        public virtual Task<TCommonEntity> GetByIdAsync<TCommonEntity>(object id)
            where TCommonEntity : class, ICommonEntity
        {
            return context.Set<TCommonEntity>().FindAsync(id);
        }

        public virtual int GetCount<TCommonEntity>(Expression<Func<TCommonEntity, bool>> filter = null)
            where TCommonEntity : class, ICommonEntity
        {
            return GetQueryable<TCommonEntity>(filter).Count();
        }

        public virtual Task<int> GetCountAsync<TCommonEntity>(Expression<Func<TCommonEntity, bool>> filter = null)
            where TCommonEntity : class, ICommonEntity
        {
            return GetQueryable<TCommonEntity>(filter).CountAsync();
        }

        public virtual bool GetExists<TCommonEntity>(Expression<Func<TCommonEntity, bool>> filter = null)
            where TCommonEntity : class, ICommonEntity
        {
            return GetQueryable<TCommonEntity>(filter).Any();
        }

        public virtual Task<bool> GetExistsAsync<TCommonEntity>(Expression<Func<TCommonEntity, bool>> filter = null)
            where TCommonEntity : class, ICommonEntity
        {
            return GetQueryable<TCommonEntity>(filter).AnyAsync();
        }
    }
}

﻿using GenRepo.Models;
using GenRepo.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GenRepo.Repository
{
    public class GenRepo<TContext> : GenRepoReadOnly<TContext>, IGenRepo
    where TContext : DbContext
    {
        public GenRepo(TContext context)
        : base(context)
        {}

        public virtual TCommonEntity Create<TCommonEntity>(TCommonEntity entity, string createdBy = null)
            where TCommonEntity : class, ICommonEntity
        {
            entity.CreatedDate = DateTime.UtcNow;
            entity.CreatedUser = 0;
            context.Set<TCommonEntity>().Add(entity);
            return entity;
        }

        public virtual TCommonEntity Update<TCommonEntity>(TCommonEntity entity, string modifiedBy = null)
            where TCommonEntity : class, ICommonEntity
        {   
            entity.ModifiedDate = DateTime.UtcNow;
            entity.ModifiedUser = 0;
            context.Set<TCommonEntity>().Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public virtual void Delete<TCommonEntity>(object id)
            where TCommonEntity : class, ICommonEntity
        {
            TCommonEntity entity = context.Set<TCommonEntity>().Find(id);
            Delete<TCommonEntity>(entity);
        }

        public virtual void Delete<TCommonEntity>(TCommonEntity entity)
            where TCommonEntity : class, ICommonEntity
        {
            var dbSet = context.Set<TCommonEntity>();
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public virtual int Save()
        {
            var validationErrors = context.ChangeTracker
                .Entries<IValidatableObject>()
                .SelectMany(e => e.Entity.Validate(null))
                .Where(r => r != ValidationResult.Success);

            if (validationErrors.Any())
            {
                ThrowEnhancedValidationException(validationErrors);
            }

            return context.SaveChanges();
        }

        public async virtual Task<int> SaveAsync()
        {
            var validationErrors = context.ChangeTracker
                .Entries<IValidatableObject>()
                .SelectMany(e => e.Entity.Validate(null))
                .Where(r => r != ValidationResult.Success);

            if (validationErrors.Any())
            {
                ThrowEnhancedValidationException(validationErrors);
            }

            return await context.SaveChangesAsync();
        }

        protected virtual void ThrowEnhancedValidationException(IEnumerable<ValidationResult> e)
        {
            string fullErrorMessage = "";
            foreach (var valErr in e)
            {
                fullErrorMessage = fullErrorMessage + valErr.ErrorMessage + "; ";
            }
            throw new Exception(fullErrorMessage);
        }
    }
}

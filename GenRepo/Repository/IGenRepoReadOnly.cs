﻿using GenRepo.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GenRepo.Repository
{
    public interface IGenRepoReadOnly
    {
        IEnumerable<TCommonEntity> GetAll<TCommonEntity>(
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity;

        Task<IEnumerable<TCommonEntity>> GetAllAsync<TCommonEntity>(
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity;

        IEnumerable<TCommonEntity> Get<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity;

        Task<IEnumerable<TCommonEntity>> GetAsync<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            int? skip = null,
            int? take = null)
            where TCommonEntity : class, ICommonEntity;

        TCommonEntity GetOne<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            IEnumerable<string> includeProperties = null)
            where TCommonEntity : class, ICommonEntity;

        Task<TCommonEntity> GetOneAsync<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            IEnumerable<string> includeProperties = null)
            where TCommonEntity : class, ICommonEntity;

        TCommonEntity GetFirst<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null)
            where TCommonEntity : class, ICommonEntity;

        Task<TCommonEntity> GetFirstAsync<TCommonEntity>(
            Expression<Func<TCommonEntity, bool>> filter = null,
            Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null)
            where TCommonEntity : class, ICommonEntity;

        TCommonEntity GetById<TCommonEntity>(object id)
            where TCommonEntity : class, ICommonEntity;

        Task<TCommonEntity> GetByIdAsync<TCommonEntity>(object id)
            where TCommonEntity : class, ICommonEntity;

        int GetCount<TCommonEntity>(Expression<Func<TCommonEntity, bool>> filter = null)
            where TCommonEntity : class, ICommonEntity;

        Task<int> GetCountAsync<TCommonEntity>(Expression<Func<TCommonEntity, bool>> filter = null)
            where TCommonEntity : class, ICommonEntity;

        bool GetExists<TCommonEntity>(Expression<Func<TCommonEntity, bool>> filter = null)
            where TCommonEntity : class, ICommonEntity;

        Task<bool> GetExistsAsync<TCommonEntity>(Expression<Func<TCommonEntity, bool>> filter = null)
            where TCommonEntity : class, ICommonEntity;
    }
}

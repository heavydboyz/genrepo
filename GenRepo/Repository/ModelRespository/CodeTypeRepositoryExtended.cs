﻿using GenRepo.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace GenRepo.Repository.ModelRepository
{
    public partial class CodeTypeRepository<TContext> : GenRepo<TContext>, IGenRepo
       where TContext : DbContext
    {
        public IEnumerable<KeyValuePair<string, string>> getAsDict(string codeType)
        {
            var results = GetFirst<CodeType>(filter: (x => x.Type == codeType));
            return results.Codes
                .Select(r => new KeyValuePair<string, string>(r.Code, r.Value));
        }
    }
}

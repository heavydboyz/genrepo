using Microsoft.EntityFrameworkCore;
namespace GenRepo.Repository.ModelRepository
{
    public partial class PersonRepository<TContext> : GenRepo<TContext>, IGenRepo
       where TContext : DbContext
    {
        public PersonRepository(TContext context)
            : base(context) {}
    }
}

using Microsoft.EntityFrameworkCore;
namespace GenRepo.Repository.ModelRepository
{
    public partial class AddressRepository<TContext> : GenRepo<TContext>, IGenRepo
       where TContext : DbContext
    {
        public AddressRepository(TContext context)
            : base(context) {}
    }
}

using Microsoft.EntityFrameworkCore;
namespace GenRepo.Repository.ModelRepository
{
    public partial class CodeTypeRepository<TContext> : GenRepo<TContext>, IGenRepo
       where TContext : DbContext
    {
        public CodeTypeRepository(TContext context)
            : base(context) {}
    }
}

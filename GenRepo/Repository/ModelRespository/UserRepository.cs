using Microsoft.EntityFrameworkCore;
namespace GenRepo.Repository.ModelRepository
{
    public partial class UserRepository<TContext> : GenRepo<TContext>, IGenRepo
       where TContext : DbContext
    {
        public UserRepository(TContext context)
            : base(context) {}
    }
}

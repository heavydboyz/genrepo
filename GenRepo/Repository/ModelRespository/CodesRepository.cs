using Microsoft.EntityFrameworkCore;
namespace GenRepo.Repository.ModelRepository
{
    public partial class CodesRepository<TContext> : GenRepo<TContext>, IGenRepo
       where TContext : DbContext
    {
        public CodesRepository(TContext context)
            : base(context) {}
    }
}

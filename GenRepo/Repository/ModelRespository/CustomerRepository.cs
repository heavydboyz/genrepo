using Microsoft.EntityFrameworkCore;
namespace GenRepo.Repository.ModelRepository
{
    public partial class CustomerRepository<TContext> : GenRepo<TContext>, IGenRepo
       where TContext : DbContext
    {
        public CustomerRepository(TContext context)
            : base(context) {}
    }
}

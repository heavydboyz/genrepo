﻿using GenRepo.Models.Entity;
using System.Threading.Tasks;

namespace GenRepo.Repository
{
    public interface IGenRepo : IGenRepoReadOnly
    {
        TCommonEntity Create<TCommonEntity>(TCommonEntity entity, string createdBy = null)
            where TCommonEntity : class, ICommonEntity;

        TCommonEntity Update<TCommonEntity>(TCommonEntity entity, string modifiedBy = null)
            where TCommonEntity : class, ICommonEntity;

        void Delete<TCommonEntity>(object id)
            where TCommonEntity : class, ICommonEntity;

        void Delete<TCommonEntity>(TCommonEntity entity)
            where TCommonEntity : class, ICommonEntity;

        int Save();

        Task<int> SaveAsync();
    }
}

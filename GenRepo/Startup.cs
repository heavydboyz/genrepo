﻿using System.IO;
using GenRepo.Models.Context;
using GenRepo.Scaffold;
using GenRepo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace GenRepo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var config = Configuration.GetSection("ApplicationSettings");

            var appSettings = new AppSettings
            {
                ConnectionStringDefault = config.GetValue<string>("ConnectionStringDefault"),
                SeedData = config.GetValue<bool>("SeedData"),
                SeedPath = config.GetValue<string>("SeedPath"),
                ControllerScaffoldConfig = config.GetValue<string>("ControllerScaffoldConfig"),
                RepositoryScaffoldConfig = config.GetValue<string>("RepositoryScaffoldConfig"),
                CreateScaffolds = config.GetValue<bool>("CreateScaffolds"),
                BaseDirectory = Directory.GetCurrentDirectory()
            };

            var Options = new DbContextOptionsBuilder<GenRepoContext>();
            Options.UseSqlServer(appSettings.ConnectionStringDefault);
            var context = new GenRepoContext(Options.Options);

            services.AddDbContext<GenRepoContext>(options =>
              options.UseSqlServer(appSettings.ConnectionStringDefault));

            if (appSettings.CreateScaffolds)
            {
                var scaffolds = new Scaffolding(appSettings, context);
            }

            services.AddSingleton<AppSettings>(appSettings);

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "GenRepo", Version = "v1" });
            });

            services.AddMvc()
                .AddJsonOptions(
                    options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddOptions();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, GenRepoContext genRepoContext, AppSettings appSettings)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.Use((context, next) => {
                context.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                return next.Invoke();
            });

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "GenRepo V1");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Application}");
            });
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().WithHeaders("authorization", "accept", "content-type", "origin"));

            genRepoContext.Seed(appSettings);
        }
    }
}

﻿using GenRepo.Models.Entity;
using System.Collections.Generic;

namespace GenRepo.Models
{
    public enum CustomerType
    {
        prospect = 1,
        customer = 2
    }
    
    public partial class Customer : CommonEntity
    {
        public CustomerType Type { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
    }
}

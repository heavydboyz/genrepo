﻿using GenRepo.Models.Configuration;
using GenRepo.Repository.ModelRepository;
using GenRepo.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;

namespace GenRepo.Models.Context
{
    public class GenRepoContext : DbContext
    {
        public DbSet<Address> Address { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Person> Person { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Codes> Codes { get; set; }
        public DbSet<CodeType> CodeType { get; set; }

        public GenRepoContext(DbContextOptions<GenRepoContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AddressConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new PersonConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new CodesConfiguration());
            modelBuilder.ApplyConfiguration(new CodeTypeConfiguration());
        }

        public void Seed(AppSettings appSettings)
        {
            if (appSettings.SeedData)
            {
                var seed = new GenRepoSeed(this, appSettings);
            }
        }
    }
}

﻿using GenRepo.Repository.ModelRepository;
using GenRepo.Services;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;

namespace GenRepo.Models.Context
{
    public class GenRepoSeed
    {
        private GenRepoContext _context;
        private AppSettings _appSettings;

        public GenRepoSeed(GenRepoContext context, AppSettings appSettings)
        {
            _context = context;
            _appSettings = appSettings;
            SeedCodeTypes();
            SeedCustomers();
            SeedUsers();
        }

        private void SeedCodeTypes()
        {
            var jsonFile = _appSettings.BaseDirectory + _appSettings.SeedPath + "\\CodeTypes.json";
            JObject codeTypes = JObject.Parse(File.ReadAllText(jsonFile));

            foreach (JObject x in codeTypes["CodeTypes"])
            {
                CodeType codeType = x.ToObject<CodeType>();
                _context.CodeType.Add(codeType);
                _context.SaveChanges();
            }
        }

        private Codes getCode(string codeType, string codeValue)
        {
            var codeRepo = new CodeTypeRepository<GenRepoContext>(_context);
            var result = codeRepo.GetFirst<CodeType>(x => x.Type == codeType);
            var codes = (List<Codes>)result.Codes;
            var code = codes.Find(x => x.Code == codeValue);
            if (code != null)
                return code;
            else
                return null;
        }

        public void SeedUsers()
        {
            var jsonFile = _appSettings.BaseDirectory + _appSettings.SeedPath + "\\user.json";
            JObject customers = JObject.Parse(File.ReadAllText(jsonFile));
            foreach (JObject x in customers["Users"])
            {
                User user = x.ToObject<User>();
                _context.User.Add(user);
                _context.SaveChanges();
            }
        }

        public void SeedCustomers()
        {
            var jsonFile = _appSettings.BaseDirectory + _appSettings.SeedPath + "\\customer.json";
            JObject customers = JObject.Parse(File.ReadAllText(jsonFile));
            foreach (JObject x in customers["Customers"])
            {
                foreach (JObject addr in x["Addresses"])
                {
                    var state = addr["State"].Value<string>().ToUpper();
                    Codes stateCode = getCode("StateCodes", state);
                    addr["State"] = JObject.FromObject(stateCode);
                }

                Customer customer = x.ToObject<Customer>();

                _context.Customer.Add(customer);
                _context.SaveChanges();
            }
        }
    }
}

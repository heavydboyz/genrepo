﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GenRepo.Models.Entity.Configuration
{
    public class CommonEntityConfiguration : IEntityTypeConfiguration<CommonEntity>
    {
        public virtual void Configure(EntityTypeBuilder<CommonEntity> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .ValueGeneratedOnAdd();

            builder.Property(c => c.CreatedDate)
                .IsRequired();

            builder.Property(c => c.LockUser).IsConcurrencyToken(true);

        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GenRepo.Models.Configuration
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
       
        public virtual void Configure(EntityTypeBuilder<Person> builder)
        {

            builder.Property(c => c.FirstName).IsRequired().HasMaxLength(30);
            builder.Property(c => c.MiddleName).HasMaxLength(30);
            builder.Property(c => c.LastName).IsRequired().HasMaxLength(30);        
        }
    }
}

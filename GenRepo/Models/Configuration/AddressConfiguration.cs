﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GenRepo.Models.Configuration
{
    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public virtual void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.Property(c => c.Address1).HasMaxLength(60).IsRequired();
            builder.Property(c => c.Address2).HasMaxLength(60);
            builder.Property(c => c.Address3).HasMaxLength(60);
            builder.Property(c => c.City).HasMaxLength(30).IsRequired();
            builder.Property(c => c.Zip).HasMaxLength(11).IsRequired();
        }
    }
}

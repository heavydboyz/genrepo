﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GenRepo.Models.Configuration
{
    public class CodesConfiguration : IEntityTypeConfiguration<Codes>
    {
        public virtual void Configure(EntityTypeBuilder<Codes> builder)
        {
            builder.Property(c => c.Code).HasMaxLength(24).IsRequired();
            builder.Property(c => c.Value).HasMaxLength(24).IsRequired();
            builder.Property(c => c.Extended).HasMaxLength(5000);
        }
    }

}

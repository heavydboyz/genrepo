﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GenRepo.Models.Configuration
{
    public class CodeTypeConfiguration : IEntityTypeConfiguration<CodeType>
    {
        public virtual void Configure(EntityTypeBuilder<CodeType> builder)
        {
            builder.HasIndex(c => c.Type);
            builder.Property(c => c.Type).HasMaxLength(24).IsRequired();
            builder.Property(c => c.Extended).HasMaxLength(5000);
        }
    }
}

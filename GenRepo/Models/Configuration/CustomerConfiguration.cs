﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GenRepo.Models.Configuration
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public virtual void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasIndex(c => c.Name);
            builder.HasIndex(c => c.Type);
            builder.HasMany(c => c.Addresses);
            builder.Property(c => c.Type).IsRequired();
            builder.Property(c => c.Name).HasMaxLength(60).IsRequired();
        }
    }
}

﻿using GenRepo.Models.Entity;

namespace GenRepo.Models
{
    public class Codes : CommonEntity
    {
        public string Code { get; set; }
        public string Value { get; set; }
        public string Extended { get; set; }
    }
}

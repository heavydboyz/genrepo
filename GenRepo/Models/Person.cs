﻿using GenRepo.Models.Entity;
using System.Collections.Generic;

namespace GenRepo.Models
{
    public partial class Person : CommonEntity
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                if (FirstName != null)
                {
                    if (MiddleName.Length > 0)
                        return FirstName + " " + MiddleName + " " + LastName;
                    else
                        return FirstName + " " + LastName;
                }
                else
                    return "";
            }
        }

        public string FirstLast
        {
            get
            {
                if (FirstName != null)
                    return FirstName + " " + LastName;
                else
                    return "";
            }
        }

        public virtual ICollection<Address> Addresses { get; set; }
    }
}

﻿using System.Collections.Generic;
using GenRepo.Models.Entity;

namespace GenRepo.Models
{
    public partial class Address : CommonEntity
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public Codes State { get; set; }
        public string Zip { get; set; }

        public string StateAbreviated
        {
            get
            {
                if (State != null)
                    return State.Code;
                else
                    return "";
            }
        }

        public string StateLong
        {
            get
            {
                if (State != null)
                    return State.Value;
                else
                    return "";
            }
        }


        public string CityStateZip
        {
            get 
            {
                return City + ", " + StateAbreviated + " " + Zip;
            }
        }

        public List<string> FormattedAddress
        {
            get
            {
                var returnValue = new List<string>();

                if (Address1 != null)
                {
                    returnValue.Add(Address1);

                    if (Address2 != null)
                    {
                        if (!Address2.Equals(""))
                            returnValue.Add(Address2);
                    }

                    if (Address3 != null)
                    {
                        if (!Address3.Equals(""))
                            returnValue.Add(Address3);
                    }
                    returnValue.Add(CityStateZip);
                }

                return returnValue;
            }
        }
    }
}

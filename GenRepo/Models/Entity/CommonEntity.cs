﻿using System;

namespace GenRepo.Models.Entity
{
    public abstract class CommonEntity : ICommonEntity
    {
        public int Id { get; set; }

        private DateTime? createdDate;
        public DateTime CreatedDate
        {
            get { return createdDate ?? DateTime.UtcNow; }
            set { createdDate = value; }
        }

        public DateTime? ModifiedDate { get; set; }
        public int CreatedUser { get; set; }
        public int ModifiedUser { get; set; }
        public int LockUser { get; set; }
    }
}

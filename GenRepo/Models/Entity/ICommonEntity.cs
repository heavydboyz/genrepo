﻿using System;

namespace GenRepo.Models.Entity
{
    public interface ICommonEntity 
    {
        int Id { get; }
        DateTime CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
        int CreatedUser { get; set; }
        int ModifiedUser { get; set; }
        int LockUser { get; set; }
    }
}

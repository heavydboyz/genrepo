﻿using GenRepo.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GenRepo.Models
{
    public class CodeType : CommonEntity
    {
        public string Type { get; set; }
        public string Extended { get; set; }

        public virtual ICollection<Codes> Codes { get; set; }
    }
}

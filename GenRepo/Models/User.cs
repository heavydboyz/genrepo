﻿using GenRepo.Models.Entity;

namespace GenRepo.Models
{
    public enum UserType
    {
        Admin = 1,
        User = 2
    }

    public partial class User : CommonEntity
    {
        public Person UserPerson { get; set; }
        public UserType Type { get; set; }
    }
}

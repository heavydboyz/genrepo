﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace GenRepo.Controllers.Response
{
    public class ResponseObject<TType> : IResponseObject<TType>
    {
        public int StatusCode { get; set; }
        public ResponseTypeCode ResponseType { get; set; }
        public IEnumerable<TType> Value { get; set; }
        public Exception Exception { get; set; }
        public TType SingularValue { get; set; }
        public Boolean IsSingular { get; set; }
        public IEnumerable<string> Messages { get; set; }

        public ResponseObject()
        {
            this.Value = new Collection<TType>();
            this.Messages = new Collection<string>();
        }

        public void CreateResponseEntity(IEnumerable<TType> value)
        {
            StatusCode = 200;
            ResponseType = ResponseTypeCode.entity;
            IsSingular = false;
            Value = value.ToList<TType>();
        }

        public void CreateResponseEntity(TType value)
        {
            StatusCode = 200;
            ResponseType = ResponseTypeCode.entity;
            IsSingular = true;
            SingularValue = value;
        }

        public void CreateResponseException(int statuscode, Exception Ex)
        {
            StatusCode = statuscode;
            ResponseType = ResponseTypeCode.exception;
            Exception = Ex;
        }

        public void CreateResponseMessage(int statuscode, string message)
        {
            StatusCode = statuscode;
            ResponseType = ResponseTypeCode.message;

            var valueCollection = new Collection<string>();
            valueCollection.Add(message);
            Messages = valueCollection;
        }

        public void CreateResponseMessages(int statuscode, IEnumerable<string> messageCollection)
        {
            StatusCode = statuscode;
            ResponseType = ResponseTypeCode.message;
            Messages = messageCollection;
        }
    }
}

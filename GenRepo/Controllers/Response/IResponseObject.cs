﻿using System;
using System.Collections.Generic;

namespace GenRepo.Controllers.Response
{
    public enum ResponseTypeCode
    {
        message = 1,
        exception = 2,
        entity = 3
    }

    public interface IResponseObject<TType> 
    {
        int StatusCode { get; set; }
        ResponseTypeCode ResponseType { get; set; }
        IEnumerable<TType> Value { get; set; }
        Exception Exception { get; set; }
        TType SingularValue { get; set; }
        Boolean IsSingular { get; set; }
        IEnumerable<string> Messages { get; set; }

        void CreateResponseEntity(IEnumerable<TType> value);
        void CreateResponseEntity(TType value);
        void CreateResponseException(int statuscode, Exception Ex);
        void CreateResponseMessage(int statusCode, string message);
        void CreateResponseMessages(int statusCode, IEnumerable<string> messageCollection);
    }
}
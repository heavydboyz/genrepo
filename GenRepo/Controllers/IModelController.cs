﻿using GenRepo.Controllers.Response;
using GenRepo.Models;
using GenRepo.Models.Entity;
using GenRepo.Repository;
using System.Threading.Tasks;

namespace GenRepo.Controllers
{
    public interface IModelController<TCommonEntity, TRepository> 
        where TCommonEntity : class, ICommonEntity 
        where TRepository : class, IGenRepo
    {
        Task<IResponseObject<TCommonEntity>> Get();
        Task<IResponseObject<TCommonEntity>> Get(int id);

        Task<IResponseObject<TCommonEntity>> Create(TCommonEntity entity);
        Task<IResponseObject<TCommonEntity>> Update(TCommonEntity entity);
        Task<IResponseObject<TCommonEntity>> Delete(int id);
        Task<IResponseObject<TCommonEntity>> Count();

        IResponseObject<TCommonEntity> GetNonAsync();
        IResponseObject<TCommonEntity> GetOneNonAsync(int id);
    }
}

﻿using GenRepo.Controllers.Response;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GenRepo.Controllers.ModelControllers
{
    public partial class CodeTypeController
    {
        [HttpGet("GetAsDict/{codeType}")]
        public async Task<IResponseObject<IEnumerable<KeyValuePair<string, string>>>> GetAsDict(string codeType)
        {
            var responseObject = new ResponseObject<IEnumerable<KeyValuePair<string, string>>>();
            try
            {
                var results = _Repo.getAsDict(codeType);
                if (results == null || results.Count() == 0)
                {
                    responseObject.CreateResponseMessage(204, "Empty DataSet!");
                    return responseObject;
                }
                else
                {
                    responseObject.CreateResponseEntity(results);
                    return responseObject;
                }
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }
    }
}

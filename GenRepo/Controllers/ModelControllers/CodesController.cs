using Microsoft.AspNetCore.Mvc;
using GenRepo.Repository.ModelRepository;
using GenRepo.Models;
using GenRepo.Models.Context;

namespace GenRepo.Controllers.ModelControllers {
   [ Route("api/[controller]") ]
    public partial class CodesController : ModelController<Codes, CodesRepository<GenRepoContext>> {
        private const string controllerName = "Codes";
        public CodesController(GenRepoContext context)
          : base(context, controllerName) {
            this._Repo = new CodesRepository<GenRepoContext>(context);
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using GenRepo.Repository.ModelRepository;
using GenRepo.Models;
using GenRepo.Models.Context;

namespace GenRepo.Controllers.ModelControllers {
   [ Route("api/[controller]") ]
    public partial class CustomerController : 
        ModelController<Customer, CustomerRepository<GenRepoContext>>
    {
        private const string controllerName = "Customer";
        public CustomerController(GenRepoContext context)
          : base(context, controllerName) {
            this._Repo = new CustomerRepository<GenRepoContext>(context);
        }
    }
}

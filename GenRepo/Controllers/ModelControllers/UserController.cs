using Microsoft.AspNetCore.Mvc;
using GenRepo.Models;
using GenRepo.Repository.ModelRepository;
using GenRepo.Models.Context;

namespace GenRepo.Controllers.ModelControllers {
   [ Route("api/[controller]") ]
    public partial class UserController : ModelController<User, UserRepository<GenRepoContext>> {
        private const string controllerName = "User";
        public UserController(GenRepoContext context)
          : base(context, controllerName) {
            this._Repo = new UserRepository<GenRepoContext>(context);
        }
    }
}

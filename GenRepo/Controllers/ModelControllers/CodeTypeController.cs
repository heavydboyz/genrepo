using Microsoft.AspNetCore.Mvc;
using GenRepo.Repository.ModelRepository;
using GenRepo.Models;
using GenRepo.Models.Context;

namespace GenRepo.Controllers.ModelControllers {
   [ Route("api/[controller]") ]
    public partial class CodeTypeController : ModelController<CodeType, CodeTypeRepository<GenRepoContext>> {
        private const string controllerName = "CodeType";
        public CodeTypeController(GenRepoContext context)
          : base(context, controllerName) {
            this._Repo = new CodeTypeRepository<GenRepoContext>(context);
        }
    }
}

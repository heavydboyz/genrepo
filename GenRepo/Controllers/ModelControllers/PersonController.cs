using Microsoft.AspNetCore.Mvc;
using GenRepo.Repository.ModelRepository;
using GenRepo.Models;
using GenRepo.Models.Context;

namespace GenRepo.Controllers.ModelControllers {
   [ Route("api/[controller]") ]
    public partial class PersonController : ModelController<Person, PersonRepository<GenRepoContext>> {
        private const string controllerName = "Person";
        public PersonController(GenRepoContext context)
          : base(context, controllerName) {
            this._Repo = new PersonRepository<GenRepoContext>(context);
        }
    }
}

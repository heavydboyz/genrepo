using Microsoft.AspNetCore.Mvc;
using GenRepo.Repository.ModelRepository;
using GenRepo.Models;
using GenRepo.Models.Context;

namespace GenRepo.Controllers.ModelControllers {
   [ Route("api/[controller]") ]
    public partial class AddressController : ModelController<Address, AddressRepository<GenRepoContext>> {
        private const string controllerName = "Address";
        public AddressController(GenRepoContext context)
          : base(context, controllerName) {
            this._Repo = new AddressRepository<GenRepoContext>(context);
        }
    }
}

﻿using GenRepo.Controllers.Response;
using GenRepo.Models.Context;
using GenRepo.Models.Entity;
using GenRepo.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GenRepo.Controllers
{
    [Route("api/[controller]")]
    public class ModelController<TCommonEntity, TRepository> : 
        Controller, 
        IModelController<TCommonEntity, TRepository>
            where TCommonEntity : class, ICommonEntity
            where TRepository : class, IGenRepo
    {
        protected GenRepoContext _Context;
        protected TRepository _Repo { get; set; }
        protected readonly string _ModelName;

        public ModelController(GenRepoContext context, string modelName)
        {
            _Context = context;
            _ModelName = modelName;
        }

        [HttpGet]
        public virtual async Task<IResponseObject<TCommonEntity>> Get()
        {
            var responseObject = new ResponseObject<TCommonEntity>();
            try
            {
                var results = await _Repo.GetAllAsync<TCommonEntity>();
                if (results == null || results.Count() == 0)
                {
                    responseObject.CreateResponseMessage(204, "Empty DataSet!");
                    return responseObject;
                }
                else
                {
                    responseObject.CreateResponseEntity(results);
                    return responseObject;
                }
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }

        [HttpGet("GetNonAsync")]
        public virtual IResponseObject<TCommonEntity> GetNonAsync()
        {
            var responseObject = new ResponseObject<TCommonEntity>();
            try
            {
                var results = _Repo.GetAll<TCommonEntity>().ToList();
                if (results == null || results.Count() == 0)
                {
                    responseObject.CreateResponseMessage(204, "Empty DataSet!");
                    return responseObject;
                }
                else
                {
                    responseObject.CreateResponseEntity(results);
                    return responseObject;
                }
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }

        [HttpGet("GetOneNonAsync")]
        public virtual IResponseObject<TCommonEntity> GetOneNonAsync(int id)
        {
            var responseObject = new ResponseObject<TCommonEntity>();
            try
            {
                var result = this._Repo.GetOne<TCommonEntity>(filter: (x => (int)x.Id == id));
                if (result == null)
                {
                    responseObject.CreateResponseMessage(204, "Empty DataSet!");
                    return responseObject;
                }
                else
                {
                    responseObject.CreateResponseEntity(result);
                    return responseObject;
                }
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }

        [HttpGet("{id:int}")]
        public async Task<IResponseObject<TCommonEntity>> Get(int id)
        {
            var responseObject = new ResponseObject<TCommonEntity>();
            try
            {
                var result = await this._Repo.GetOneAsync<TCommonEntity>(filter: (x => (int)x.Id == id));
                if (result == null)
                {
                    responseObject.CreateResponseMessage(204, "Empty DataSet!");
                    return responseObject;
                }
                else
                {
                    responseObject.CreateResponseEntity(result);
                    return responseObject;
                }
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }

        [HttpPost]
        public async Task<IResponseObject<TCommonEntity>> Create(TCommonEntity entity)
        {
            var responseObject = new ResponseObject<TCommonEntity>();
            try
            {
                var returnEntity = this._Repo.Create<TCommonEntity>(entity);
                var result = await this._Repo.SaveAsync();
                responseObject.CreateResponseEntity(returnEntity);
                return responseObject;
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }

        [HttpPut]
        public async Task<IResponseObject<TCommonEntity>> Update(TCommonEntity entity)
        {
            var responseObject = new ResponseObject<TCommonEntity>();
            try
            {
                var returnEntity = this._Repo.Update<TCommonEntity>(entity);
                var result = await this._Repo.SaveAsync();
                responseObject.CreateResponseEntity(returnEntity);
                return responseObject;
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }

        [HttpDelete]
        public async Task<IResponseObject<TCommonEntity>> Delete(int id)
        {
            var responseObject = new ResponseObject<TCommonEntity>();
            try
            {
                this._Repo.Delete<TCommonEntity>(id);
                var result = await this._Repo.SaveAsync();
                if (result != 1)
                {
                    responseObject.CreateResponseMessage(204, "Nothing Deleted!");
                    return responseObject;
                }
                {
                    responseObject.CreateResponseMessage(200, "Entity Deleted!");
                    return responseObject;
                }
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }

        [HttpGet("Count")]
        public async Task<IResponseObject<TCommonEntity>> Count()
        {
            var responseObject = new ResponseObject<TCommonEntity>();
            try
            {
                var result = await this._Repo.GetCountAsync<TCommonEntity>();
                responseObject.CreateResponseMessage(204, result.ToString());
                return responseObject;
            }
            catch (Exception Ex)
            {
                responseObject.CreateResponseException(400, Ex);
                return responseObject;
            }
        }
    }
}

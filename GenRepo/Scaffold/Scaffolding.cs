﻿using GenRepo.Models.Context;
using GenRepo.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GenRepo.Scaffold
{
    public enum scaffoldType
    {
        controller = 1,
        respository = 2
    }

    public enum surrogateType
    {
        input = 1,
        value = 2
    }

    public class Surrogate
    {
        public string Name { get; set; }
        public surrogateType Type { get; set; }
        public string Value { get; set; }
    }

    public class Scaffold
    {
        public scaffoldType Type { get; set; }
        public string Name { get; set; }
        public string OutputPath { get; set; }
        public string SurrogateIdentifier { get; set; }
        public List<Surrogate> Surrogates { get; set; }
        public List<string> Code { get; set; }
    }

    public class Scaffolding
    {
        private List<Scaffold> _scaffolds;
        private AppSettings _appSettings;
        private GenRepoContext _context;

        public Scaffolding(AppSettings appSettings, GenRepoContext context)
        {
            _appSettings = appSettings;
            _scaffolds = new List<Scaffold>();
            _context = context;

            _scaffolds.Add(readScaffold(scaffoldType.controller));
            _scaffolds.Add(readScaffold(scaffoldType.respository));

            writeScaffold(scaffoldType.controller);
            writeScaffold(scaffoldType.respository);
        }

        private Scaffold readScaffold(scaffoldType type)
        {
            string jsonFile;

            switch (type)
            {
                case scaffoldType.controller:
                    jsonFile = _appSettings.BaseDirectory + _appSettings.ControllerScaffoldConfig;
                    break;

                case scaffoldType.respository:
                    jsonFile = _appSettings.BaseDirectory + _appSettings.RepositoryScaffoldConfig;
                    break;

                default:
                    return null;
            }

            if (File.Exists(jsonFile))
            {
                try
                {
                    using (StreamReader reader = File.OpenText(jsonFile))
                    {
                        JObject o = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                        Scaffold scaffold = o.ToObject<Scaffold>();
                        return scaffold;
                    }
                }
                catch (Exception Ex)
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        private void writeScaffold(scaffoldType type)
        {
            Scaffold scaffold = _scaffolds.Find(s => s.Type == type);
            string jsonDir = _appSettings.BaseDirectory + scaffold.OutputPath;

            var fileNamePostFix = "";

            switch (type)
            {
                case scaffoldType.controller:
                    fileNamePostFix = "Controller.cs";
                    break;

                case scaffoldType.respository:
                    fileNamePostFix = "Repository.cs";
                    break;

                default:
                    return;
            }


            if (scaffold != null)
            {
                foreach (var entityType in _context.Model.GetEntityTypes())
                {
                    var tableName = entityType.Relational().TableName;

                    var fileName = jsonDir + tableName + fileNamePostFix;
                    List<string> writeLines = new List<string>();

                    var tableSurrogate = scaffold.Surrogates.Find(s => s.Name.Equals("table"));
                    if (tableSurrogate != null)
                        tableSurrogate.Value = tableName;

                    scaffold.Code.ForEach(c =>
                    {
                        StringBuilder curLine = new StringBuilder().Append(c);

                        scaffold.Surrogates.ForEach(s =>
                        {
                            curLine.Replace(scaffold.SurrogateIdentifier + s.Name + scaffold.SurrogateIdentifier, s.Value);
                        });

                        curLine.Replace(@"\{", @"{");
                        curLine.Replace(@"\}", @"}");
                        curLine.Replace(@"\[", @"[");
                        curLine.Replace(@"\]", @"]");

                        writeLines.Add(curLine.ToString());

                    });

                    try
                    {
                        using (StreamWriter file = new System.IO.StreamWriter(fileName))
                        {
                            writeLines.ForEach(l => file.WriteLine(l));
                        }
                    }
                    catch (Exception Ex)
                    {

                    }
                }
            }
        }
    }
}

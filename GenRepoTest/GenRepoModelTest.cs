using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GenRepo.Controllers;
using GenRepo.Controllers.ModelControllers;
using GenRepo.Controllers.Response;
using GenRepo.Models.Context;
using GenRepo.Models.Entity;
using GenRepo.Repository;
using GenRepo.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace GenRepoTest
{
    public class GenRepoModelTest<TCommonEntity, TGenRepo>
        where TCommonEntity : class, ICommonEntity
        where TGenRepo : class, IGenRepo
    {

        public IModelController<TCommonEntity, TGenRepo> _controller;
        public int _recCount;
        public string _model;

        public async Task Get()
        {
            try
            {
                var result = await _controller.Get();
                var responseResult = Assert.IsAssignableFrom<IResponseObject<TCommonEntity>>(result);
                var entity = Assert.IsAssignableFrom<IEnumerable<TCommonEntity>>(result.Value);
                Assert.Equal(_recCount, entity.Count());
                Console.WriteLine(String.Format("Test Get Passed for {0} ", _model));
            }       
            catch(Exception ex)
            {
                Console.WriteLine(String.Format("Test Get Failed for {0} {1}", _model, ex.Message));
            }
        }

        public async Task GetById(int id)
        {
            try
            {
                var result = await _controller.Get(id);
                var responseResult = Assert.IsAssignableFrom<IResponseObject<TCommonEntity>>(result);
                var entity = Assert.IsAssignableFrom<TCommonEntity>(result.SingularValue);
                Console.WriteLine("Test GetById Passed for {0} ", _model);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Test GetById Failed for {0} {1}", _model, ex.Message);
            }
        }

        public async Task Create(TCommonEntity newEntity)
        {
            try
            {
                var result = await _controller.Create(newEntity);
                var responseResult = Assert.IsAssignableFrom<IResponseObject<TCommonEntity>>(result);
                var entity = Assert.IsAssignableFrom<TCommonEntity>(result.SingularValue);
                Console.WriteLine("Test Create Passed for {0} ", _model);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Test Create Failed for {0} {1}", _model, ex.Message);
            }
        }

        public async Task Update(TCommonEntity newEntity)
        {
            try
            {
                var result = await _controller.Update(newEntity);
                var responseResult = Assert.IsAssignableFrom<IResponseObject<TCommonEntity>>(result);
                var entity = Assert.IsAssignableFrom<TCommonEntity>(result.SingularValue);
                Console.WriteLine("Test Update Passed for {0} ", _model);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Test Create Failed for {0} {1}", _model, ex.Message);
            }
        }

        public async Task Delete(int id)
        {
            try
            {
                var result = await _controller.Delete(id);
                var responseResult = Assert.IsAssignableFrom<IResponseObject<string>>(result);
                var entity = Assert.IsAssignableFrom<IEnumerable<string>>(result.Value);
                Console.WriteLine("Test Delete Passed for {0} ", _model);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Test Delete Failed for {0} {1}", _model, ex.Message);
            }
        }

    }
}


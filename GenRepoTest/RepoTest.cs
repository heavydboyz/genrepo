using System;
using System.Collections.ObjectModel;
using System.IO;
using GenRepo.Controllers;
using GenRepo.Controllers.ModelControllers;
using GenRepo.Models;
using GenRepo.Models.Context;
using GenRepo.Repository.ModelRepository;
using GenRepo.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace GenRepoTest
{
    public class GenRepoTest
    {
        AppSettings _appSettings;
        IConfiguration Configuration;
        GenRepoContext _context;
        
        [Fact]
        public void TestCustomerGet()
        {
            getAppSetting();
            getContext();

            var customerTest = new GenRepoModelTest<Customer, CustomerRepository<GenRepoContext>>
            {
                _controller = new CustomerController(_context),
                _recCount = 28,
                _model = "Customer"
            };
            customerTest.Get().Wait();
        }

        [Fact]
        public void TestCustomerGetById()
        {
            getAppSetting();
            getContext();

            var customerTest = new GenRepoModelTest<Customer, CustomerRepository<GenRepoContext>>
            {
                _controller = new CustomerController(_context),
                _model = "Customer"
            };
            customerTest.GetById(1).Wait();
        }

        [Fact]
        public void TestCustomerCreate()
        {
            getAppSetting();
            getContext();

            var address = new Address
            {
                Address1 = "101 Street",
                City = "Springfield",
                Zip = "62704"
            };

            var customer = new Customer
            {
                Name = "Unit Testing",
                Type = CustomerType.customer,
            };

            customer.Addresses = new Collection<Address>();
            customer.Addresses.Add(address);

            var customerTest = new GenRepoModelTest<Customer, CustomerRepository<GenRepoContext>>
            {
                _controller = new CustomerController(_context),
                _model = "Customer"
            };
            customerTest.Create(customer).Wait();
        }

        [Fact]
        public void TestCustomerUpdate()
        {
            getAppSetting();
            getContext();

            var customer = _context.Customer.Find(2);
            customer.Name = "Unit Testing is Fun";

            var customerTest = new GenRepoModelTest<Customer, CustomerRepository<GenRepoContext>>
            {
                _controller = new CustomerController(_context),
                _model = "Customer"
            };
            customerTest.Update(customer).Wait();
        }

        [Fact]
        public void TestCustomerDelete()
        {
            getAppSetting();
            getContext();

            var customerTest = new GenRepoModelTest<Customer, CustomerRepository<GenRepoContext>>
            {
                _controller = new CustomerController(_context),
                _model = "Customer"
            };

            customerTest.Delete(2).Wait();
        }

        private void getAppSetting()
        {
            var builder = new ConfigurationBuilder()
                        .AddJsonFile(@"C:\Users\mdesalle.EARNESTASSOC\Documents\Visual Studio 2017\Projects\GenRepo\GenRepo\appsettings.json");

            Configuration = builder.Build();

            var config = Configuration.GetSection("ApplicationSettings");

            _appSettings = new AppSettings
            {
                ConnectionStringDefault = config.GetValue<string>("ConnectionStringDefault"),
                SeedData = config.GetValue<bool>("SeedData"),
                SeedPath = config.GetValue<string>("SeedPath"),
                ControllerScaffoldConfig = config.GetValue<string>("ControllerScaffoldConfig"),
                RepositoryScaffoldConfig = config.GetValue<string>("RepositoryScaffoldConfig"),
                CreateScaffolds = config.GetValue<bool>("CreateScaffolds"),
                BaseDirectory = Directory.GetCurrentDirectory()
            };
        }

        private void getContext()
        {
            var Options = new DbContextOptionsBuilder<GenRepoContext>();
            Options.UseSqlServer(_appSettings.ConnectionStringDefault);
            _context = new GenRepoContext(Options.Options);
        }

    }
}

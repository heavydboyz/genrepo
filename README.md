# GenRepo #

WebApi .Net 2.0 Core Entity Framwork Generic Respository with 
Model Controller Framework

### What is GenRepo? ###

* Define your Entiy Framework data models
* Start application
* Repository and controllers will be created

### Setup ###

* Clone Repo
* Visual Studio
* Update appsettings.json with connection string
* Start Application
* Test it out with Swagger 

###  How it Works ###

* The project is based on the Models.Entity.ICommonEntity Interface where all the EF data models use its implmentation.  



        public interface ICommonEntity 
        {
            int Id { get; }
            DateTime CreatedDate { get; set; }
            DateTime? ModifiedDate { get; set; }
            int CreatedUser { get; set; }
            int ModifiedUser { get; set; }
            int LockUser { get; set; }
        }



* The ICommonEntity interface allows for the Repository.IGenRepo and Repository.IGenRepoReadOnly interface to implement CRUD operations on a particular model. 



		public interface IGenRepoReadOnly
        { 
            IEnumerable<TCommonEntity> GetAll<TCommonEntity>(
                Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
                IEnumerable<string> includeProperties = null,
                int? skip = null,
                int? take = null)
                where TCommonEntity : class, ICommonEntity;

            Task<IEnumerable<TCommonEntity>> GetAllAsync<TCommonEntity>(
                Func<IQueryable<TCommonEntity>, IOrderedQueryable<TCommonEntity>> orderBy = null,
                IEnumerable<string> includeProperties = null,
                int? skip = null,
                int? take = null)
                where TCommonEntity : class, ICommonEntity;
            ...

        public interface IGenRepo : IGenRepoReadOnly
        {
            TCommonEntity Create<TCommonEntity>(TCommonEntity entity, string createdBy = null)
                where TCommonEntity : class, ICommonEntity;

            TCommonEntity Update<TCommonEntity>(TCommonEntity entity, string modifiedBy = null)
                where TCommonEntity : class, ICommonEntity;
            ...



* In theory the IGenRepo interface could be used for all CRUD operations for all models that implement the ICommonEntity interface. In practice this is limiting and not the most workable solution. Therefore the projects uses scaffloding for each model to create a partial class that inherites from the Repository.GenRepo Class. Giving the model basic CRUD operations along with the ability to define another partial class with any unique operations that will be needed. Refer to Repository.ModelRepository.CodeTypeExtended for an example. The scaffloding occurs on Application Startup.     

* Using the IGenRepo interface allows for the creation of your model controllers using the Controllers.IModelController interface where your API routes are defined and implemented.


        public interface IModelController<TCommonEntity, TRepository> 
            where TCommonEntity : class, ICommonEntity 
            where TRepository : class, IGenRepo
        {
            Task<IResponseObject<TCommonEntity>> Get();
            Task<IResponseObject<TCommonEntity>> Get(int id);
            Task<IResponseObject<TCommonEntity>> Create(TCommonEntity entity);
            Task<IResponseObject<TCommonEntity>> Update(TCommonEntity entity);
            Task<IResponseObject<string>> Delete(int id);
            Task<IResponseObject<TCommonEntity>> Count();
            IResponseObject<TCommonEntity> GetNonAsync();
            IResponseObject<TCommonEntity> GetOneNonAsync(int id);
        }


* Scafollding again is used to create a controller partial class for each EF data model. The scaffolding is created on application start.


